# Template Breadcrumb

This module allows a breadcrumb to be inserted into a nodes display settings, so it can be output as a field in the template using `content.template_breadcrumb`.

## Depenency

This module relies on the following patch for `easy_breadcrumb`: https://www.drupal.org/project/easy_breadcrumb/issues/2979021